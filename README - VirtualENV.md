#VirtualEnv - README            
      
***************************************************************************************************       
        
Create environment - virtualenv -p /usr/bin/python3.4 env        
Switch to environment - source env/bin/activate     
Install Requirements - pip install -r requirements.txt         
         
Create requirements - pip freeze > requirements.txt        
         
Exit from environment - deactivate        
           
***************************************************************************************************       