#Deployment ReadMe     
     
The supported deployment platform at the moment is, Ubuntu 14.04.		
Deployments are on Apache / MySQL at the moment. Will eventually move to NginX and Gunicorn and PostGre.        
     
##Initial Deployment:						
    
***************************************************************************************************         
          
	Upload src/core and src/media to any directory on production		 	
		
	Install MySQL database         
		- Run, sudo apt-get install mysql-server    
		
	Manually install prerequisite's mentioned in script.sh	    
		
	cd to directory CONTAINING core  	
			
	Update /core/web/settings.py with production specific details. Sample production settings at src/settings.py.prod       
		- Set Debug = False        
		- Set AllowedHosts			
		- Update database credentials          
		- Update MEDIA_ROOT			
		
	Switch to virtualenv, and install dependencies   	
		- Run, virtualenv -p /usr/bin/python3.4 env        
		- Run, source env/bin/activate    
		- Run, pip install -r requirements.txt       
		
	Run, python core/manage.py makemigrations          
	Run, python core/manage.py migrate          
	Run, python core/manage.py createsuperuser      
		
	Configure with Apache           
		- Run, python core/manage.py collectstatic    		
		- Configure virtual host on Apache with configurations on the lines of,			
			src/000-default.conf		
		- Run, sudo /python_path/bin/mod_wsgi-express install-module		
		- Edit WSGI configurations		
			vi /etc/apache2/mods-available/wsgi.load			
				LoadModule wsgi_module /usr/lib/apache2/modules/mod_wsgi-py34.cpython-34m.so			
		- Restart Apache		
		
	Dawn can be accessed at http://<virtual-host-mapping>      
	Admin can be accessed at http://<virtual-host-mapping>/admin		
		- Add pages - about, work, contact, and also some blog posts from the admin, and see them turn to life through the app ;)	  
		
    To exit from virtualenv, just run the command 'deactivate'  	
	
***************************************************************************************************         
     
## Incremental deployments     
        
***************************************************************************************************      		
		
Upload updates in src/core and src/webroot     
	- Have a backup of /core/web/settings.py, /core/web/urls.py. Ensure this is not overwritten      
	
Run, pip install -r requirements.txt, if PIP prerequisites are updated             
      
Restart Apache            
	   
***************************************************************************************************      
         
