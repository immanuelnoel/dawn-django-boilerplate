from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import logout

def login(request):
    context = {'page_sidebar': 'false'}
    return render(request, 'gatekeeper/login.html', context)

def doLogin(request):
    try:
        if request.POST['newuser']:
            # Create user and log in automatically
            user = User.objects.create_user(request.POST['username'], request.POST['username'], request.POST['password'])
            user.save()
            
            messages.success(request, "Thanks for signing up! Hope you have a good time around here!")
            return HttpResponseRedirect(reverse('gatekeeper:dashboard'))
        else:
            # Login existing user
            user = authenticate(username=request.POST['username'], password=request.POST['password'])
            if user is not None:
                # the password verified for the user
                if user.is_active:
                    messages.success(request, "Thanks for returning!")
                    
                    # Redirect to Dashboard
                    return HttpResponseRedirect(reverse('gatekeeper:dashboard'))    
                else:
                    messages.success(request, "The password is valid, but the account has been disabled!")
                    return HttpResponseRedirect(reverse('gatekeeper:login'))
            else:
                # the authentication system was unable to verify the username and password
                messages.success(request, "Username and password are incorrect.")
                return HttpResponseRedirect(reverse('gatekeeper:login'))
            
    except (KeyError, any):
        messages.error(request, "We don't really understand the entered information. Can you please retry?")
        return HttpResponseRedirect(reverse('gatekeeper:login'))
        
def doLogout(request):
    # Clear session and redirect to home
    logout(request)
    return HttpResponseRedirect(reverse('pages:index'))
        
def edit(request):
    context = {'page_sidebar': 'false'}
    return render(request, 'gatekeeper/edit.html', context)
    
def dashboard(request):
    context = {'page_sidebar': 'false'}
    return render(request, 'gatekeeper/dashboard.html', context)