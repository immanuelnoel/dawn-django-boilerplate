from django.conf.urls import patterns, url
 
from gatekeeper import views 
 
urlpatterns = patterns('',
    # Absolute definitions go first
    url(r'^login', views.login, name='login'),
	url(r'^doLogin', views.doLogin, name='doLogin'),
    url(r'^logout', views.doLogout, name='logout'),
    url(r'^edit', views.edit, name='edit'),
    url(r'^dashboard', views.dashboard, name='dashboard'),
    
)