from django.contrib import admin
from pages.models import Page, Message

admin.site.register(Page)
admin.site.register(Message)
