from django import forms

from .models import Message

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('message_name', 'message_email', 'message_web', 'message_details',)