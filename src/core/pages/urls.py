from django.conf.urls import patterns, url
 
from pages import views 
 
urlpatterns = patterns('',
    # Absolute definitions go first
    url(r'^$', views.index, name='index'),
    url(r'^contact', views.contact, name='contact'),
    url(r'^message', views.message, name='message'),
    url(r'^(?P<page_slug>[a-z]+)/$', views.page, name='page'),
)