# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_auto_20150111_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='message_created_date',
            field=models.DateTimeField(default=None),
            preserve_default=True,
        ),
    ]
