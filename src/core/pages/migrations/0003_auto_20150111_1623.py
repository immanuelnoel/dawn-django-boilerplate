# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20150111_1512'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='page_is_contact',
        ),
        migrations.RemoveField(
            model_name='page',
            name='page_is_login',
        ),
    ]
