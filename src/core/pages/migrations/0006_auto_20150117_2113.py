# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0005_auto_20150111_2345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='message_created_date',
            field=models.DateTimeField(default=None, help_text=b'Date/Time of the post of this message'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='message_details',
            field=models.TextField(help_text=b'What do you want to tell us?', max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='message_email',
            field=models.EmailField(help_text=b'We promise not to publicize it', max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='message_name',
            field=models.CharField(help_text=b'How would you like us to call you?', max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='message_web',
            field=models.URLField(help_text=b'Your website URL', max_length=50, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='page',
            name='page_content',
            field=models.TextField(help_text=b'Page content', max_length=1000),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='page',
            name='page_created_date',
            field=models.DateTimeField(help_text=b'Date of creation of the page'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='page',
            name='page_name',
            field=models.CharField(help_text=b'Page Name', max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='page',
            name='page_slug',
            field=models.SlugField(help_text=b'This is the URL friendly page name', max_length=80),
            preserve_default=True,
        ),
    ]
