# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0004_message_message_created_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='contact_message',
            new_name='message_details',
        ),
        migrations.RenameField(
            model_name='message',
            old_name='contact_email',
            new_name='message_email',
        ),
        migrations.RenameField(
            model_name='message',
            old_name='contact_name',
            new_name='message_name',
        ),
        migrations.RenameField(
            model_name='message',
            old_name='contact_web',
            new_name='message_web',
        ),
    ]
