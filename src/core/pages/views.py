from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib import messages

import datetime

from pages.models import Page, Message
from pages.forms import MessageForm

def index(request):
    return render(request, 'pages/index.html')

def page(request, page_slug):
	# Holder for dynamically fetched content
    current_page = get_object_or_404(Page, page_slug=page_slug)
    context = {'content': current_page, 'page_sidebar': 'true'}
    return render(request, 'pages/page.html', context)
	
def contact(request):
    current_page = get_object_or_404(Page, page_slug='contact')
	
    # Holder for dynamically generated ModelForm
    form = MessageForm()
    context = {'content': current_page, 'form': form, 'page_sidebar': 'false'}
    return render(request, 'pages/contact.html', context)
	
def message(request):
    try:
        m = Message.objects.create(message_name=request.POST['name'], message_email=request.POST['email'], message_web=request.POST['web'], message_details=request.POST['message'], message_created_date=datetime.datetime.now())
    except (KeyError, Message.DoesNotExist):
        messages.error(request, "Please recheck the entered information, and submit again")
        return render(request, 'pages/contact.html')
    else:
        Message.save(m)
        messages.success(request, "Thank you for leaving a note!")
        return HttpResponseRedirect(reverse('pages:contact'))