from django.db import models

class Page(models.Model):
    page_slug = models.SlugField(max_length=80, help_text="This is the URL friendly page name")
    page_name = models.CharField(max_length=50, help_text="Page Name")
    page_created_date = models.DateTimeField(help_text="Date of creation of the page")
    page_content = models.TextField(max_length=1000, help_text="Page content")
    
    def __unicode__(self):
	    return self.page_name

class Message(models.Model):
    message_name = models.CharField(max_length=50, help_text="How would you like us to call you?")
    message_email = models.EmailField(max_length=50, help_text="We promise not to publicize it")
    message_web = models.URLField(max_length=50, blank=True, help_text="Your website URL")
    message_details = models.TextField(max_length=500, help_text="What do you want to tell us?")
    message_created_date = models.DateTimeField(default=None, help_text="Date/Time of the post of this message")
	
    def __unicode__(self):
	    return self.message_email
