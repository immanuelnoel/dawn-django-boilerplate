from django.shortcuts import render, get_object_or_404
from blog.models import Post

def index(request):
    posts = Post.objects.order_by('post_created_date').reverse()[:10]
    context = {'posts': posts}
    return render(request, 'blog/index.html', context)
	
def post(request, post_slug):
    post = get_object_or_404(Post, post_slug=post_slug)
    return render(request, 'blog/post.html', {'post': post})
