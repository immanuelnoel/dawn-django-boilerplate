from django.conf.urls import patterns, url

from blog import views

urlpatterns = patterns('',
	url(r'^(?P<post_slug>[a-z]+)/$', views.post, name='post'),
    url(r'^$', views.index, name='index'),
)