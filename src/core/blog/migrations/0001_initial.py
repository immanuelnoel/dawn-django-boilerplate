# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment_name', models.CharField(help_text=b'How would you like us to call you?', max_length=50)),
                ('comment_email', models.EmailField(help_text=b'We promise not to publicize it', max_length=50)),
                ('comment_web', models.URLField(help_text=b'Your website URL', max_length=50, blank=True)),
                ('comment_details', models.TextField(help_text=b'Text in your comment', max_length=500)),
                ('comment_created_date', models.DateTimeField(default=None, help_text=b'Date/Time of the post of this comment')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_slug', models.SlugField(help_text=b'This is the URL friendly Post name', max_length=80)),
                ('post_name', models.CharField(help_text=b'Post Name', max_length=50)),
                ('post_content', models.TextField(help_text=b'Post content', max_length=5000)),
                ('post_created_date', models.DateTimeField(help_text=b'Date of creation of the Post')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
