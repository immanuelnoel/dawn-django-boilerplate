from django.db import models

# Create your models here.
class Post(models.Model):
    post_slug = models.SlugField(max_length=80, help_text="This is the URL friendly Post name")
    post_name = models.CharField(max_length=50, help_text="Post Name")
    post_content = models.TextField(max_length=5000, help_text="Post content")
    post_created_date = models.DateTimeField(help_text="Date of creation of the Post")
    
    def __unicode__(self):
	    return self.post_name

class Comment(models.Model):
    comment_name = models.CharField(max_length=50, help_text="How would you like us to call you?")
    comment_email = models.EmailField(max_length=50, help_text="We promise not to publicize it")
    comment_web = models.URLField(max_length=50, blank=True, help_text="Your website URL")
    comment_details = models.TextField(max_length=500, help_text="Text in your comment")
    comment_created_date = models.DateTimeField(default=None, help_text="Date/Time of the post of this comment")
	
    def __unicode__(self):
	    return self.comment_details