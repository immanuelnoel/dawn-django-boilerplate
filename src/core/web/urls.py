from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
	url(r'^account/', include('gatekeeper.urls', namespace="gatekeeper")),
    url(r'^blog/', include('blog.urls', namespace="blog")),
    url(r'', include('pages.urls', namespace="pages")),
	
    # Application Specific definitions
    #url(r'^i/', include('personal.urls', namespace="personal")),	
)
