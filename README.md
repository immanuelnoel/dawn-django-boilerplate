#D A W N  
           
##Dawn is boilerplate for creating interactive web applications with Python/Django, and bundles the below essentials    
       
###Platform       
    
***************************************************************************************************         
        
 -> Vagrant, virtualenv ready	
 -> Python 3    
 -> Django 1.8     
 -> ModWSGI with Apache 2.4.x	  
 -> Foundation Framework - JQuery built in   
     
 * Included scripts tested on Ubuntu 14.04, but may be supported on other Ubuntu distros	 
       
***************************************************************************************************          
       
###Features        
Either in-house implementation, or integrates a public available, open source package      
***************************************************************************************************         
      
 -> Pages - Support for website's static content, along with handlers for form submissions such as 'contact'      
 -> Blog - Basic functionalities for blog.      
 -> Admin - Wordpress style administrator support (in the works)    
 -> Authentication and Authorization (Open Source)         
 -> User Management - Authentication and Authorization                
           
***************************************************************************************************           
		
Sample deployment: http://dawn.inoel.in/   
		 
##Start by:         
***************************************************************************************************         
            
	Install MySQL database         
	- Run, sudo apt-get install mysql-server      
    	
	(Prerequisite)		
	Using Vagrant - 		
		- Update Vagrant specifics in Vagrant file        
		- Run, 'vagrant up' from a command line launched as administrator     
    	
    Without Vagrant -         
		- Deploy code within src into a directory on Ubuntu, say, /opt/Dawn        
		- Manually install prerequisite's mentioned in script.sh      
   			
	cd to /opt/Dawn         
	Update /core/web/settings.py with app specific details     
	Switch to virtualenv, and install dependencies   
		- Run, virtualenv -p /usr/bin/python3.4 env        
		- Run, source env/bin/activate    
		- Run, pip install -r requirements.txt       
		- See README - VirtualENV.md for more virtualEnv operations           
	cd core/      
	Run, python core/manage.py makemigrations          
	Run, python manage.py migrate          
	Run, python manage.py createsuperuser          
	
	Run Server 
		- Run, python manage.py runserver 0.0.0.0:8000            
    
	Dawn can be accessed at http://<machine-ip>:8000/      
	Admin can be accessed at http://<machine-ip>:8000/admin
		- Add pages - about, work, contact, and also some blog posts from the admin, and see them turn to life through the app ;)	  
		
    To exit from virtualenv, just run the command 'deactivate'  	
		
	See README - Deployment.md for deployment instructions		
	   
***************************************************************************************************      
         
