#VAGRANT - README    
         
The Django application is deployed over vagrant. We use a Ubuntu 14.x here. Vagrant needs to be up'd from the command line run as an administrator.      
The vagrant configurations you would want to worry about, are mentioned below. 
       
##Custom Configurations:         
***************************************************************************************************         
	
	config.vm.synced_folder - Defaults to /opt        		
	config.vm.provision - script.sh - Installs bare necessities. Django is installed within virtualenv later.         	
	
***************************************************************************************************         
		
		